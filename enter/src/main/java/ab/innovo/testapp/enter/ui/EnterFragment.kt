package ab.innovo.testapp.enter.ui

import ab.innovo.testapp.MainViewModel
import ab.innovo.testapp.common.BaseFragment
import ab.innovo.testapp.common.viewBinding
import ab.innovo.testapp.enter.R
import ab.innovo.testapp.enter.databinding.FragmentEnterBinding
import androidx.fragment.app.activityViewModels

class EnterFragment : BaseFragment(R.layout.fragment_enter) {

    private val binding: FragmentEnterBinding by viewBinding(FragmentEnterBinding::bind)
    private val viewModel: MainViewModel by activityViewModels()

    override fun onInitUI(firstInit: Boolean) {
        super.onInitUI(firstInit)
        changeButtonState()
    }

    private fun changeTextValue(value: Int) {
        var textViewValue: Int = binding.tvValue.text.toString().toIntOrNull() ?: 0
        textViewValue += value
        viewModel.setPercentage(textViewValue)
    }

    private fun changeButtonState() = with(binding) {
        val textViewValue: Int = tvValue.text.toString().toIntOrNull() ?: 0
        btnDecrease.isEnabled = textViewValue > 0
        btnIncrease.isEnabled = textViewValue < 100
    }

    override fun onObserveViewModel() {
        super.onObserveViewModel()

        viewModel.percentage.observe(viewLifecycleOwner) {
            binding.tvValue.text = it.toString()
            changeButtonState()
        }
    }

    override fun onSetOnClickListeners() = with(binding) {
        super.onSetOnClickListeners()

        btnDecrease.setOnClickListener {
            changeTextValue(-10)
        }

        btnIncrease.setOnClickListener {
            changeTextValue(10)
        }
    }
}