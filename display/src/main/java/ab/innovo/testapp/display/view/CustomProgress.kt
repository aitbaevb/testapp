package ab.innovo.testapp.display.view

import ab.innovo.testapp.R
import android.content.Context
import android.content.res.Resources.getSystem
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import kotlin.math.acos


class CustomProgress(context: Context, attrs: AttributeSet?, defStyleAttr: Int):
    View(context, attrs, defStyleAttr) {
    private var textPaint: Paint = TextPaint()
    private val rectF = RectF()
    var max = 100
    var progress = 0
        set(value) {
            field = if (value > max) {
                max
            } else
                value
            invalidate()
        }
    private val paint: Paint = Paint()

    init {
        textPaint.color = ContextCompat.getColor(context, R.color.white)
        textPaint.textSize = (18 * getSystem().displayMetrics.density).toInt().toFloat()
        textPaint.isAntiAlias = true
        paint.isAntiAlias = true
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    private val drawText: String
        get() = "$progress %"

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        rectF[0f, 0f, MeasureSpec.getSize(widthMeasureSpec).toFloat()] = MeasureSpec.getSize(heightMeasureSpec).toFloat()
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas) {
        val yHeight = progress / max.toFloat() * height
        val radius = width / 2f
        val angle = (acos(((radius - yHeight) / radius).toDouble()) * 180 / Math.PI).toFloat()
        val startAngle = 90 + angle
        val sweepAngle = 360 - angle * 2
        paint.color = ContextCompat.getColor(context, R.color.grey)
        canvas.drawArc(rectF, startAngle, sweepAngle, false, paint)
        canvas.save()
        canvas.rotate(180f, (width/2).toFloat(), (height/2).toFloat())
        paint.color = ContextCompat.getColor(context, R.color.purple_700)
        canvas.drawArc(rectF, 270 - angle, angle * 2, false, paint)
        canvas.restore()

        val textHeight: Float = textPaint.descent() + textPaint.ascent()
        canvas.drawText(drawText, (width - textPaint.measureText(drawText)) / 2.0f, (width - textHeight) / 2.0f, textPaint)
    }
}