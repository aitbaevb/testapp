package ab.innovo.testapp.display.ui

import ab.innovo.testapp.MainViewModel
import ab.innovo.testapp.common.BaseFragment
import ab.innovo.testapp.common.viewBinding
import ab.innovo.testapp.display.R
import ab.innovo.testapp.display.databinding.FragmentDisplayBinding
import androidx.fragment.app.activityViewModels

class DisplayFragment: BaseFragment(R.layout.fragment_display) {

    private val binding: FragmentDisplayBinding by viewBinding(FragmentDisplayBinding::bind)
    private val viewModel: MainViewModel by activityViewModels()

    override fun onObserveViewModel() {
        super.onObserveViewModel()

        viewModel.percentage.observe(viewLifecycleOwner) {
            binding.progress.progress = it
        }
    }
}