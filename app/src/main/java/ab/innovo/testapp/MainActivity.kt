package ab.innovo.testapp

import ab.innovo.testapp.common.BaseActivity
import ab.innovo.testapp.databinding.ActivityMainBinding
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController

class MainActivity : BaseActivity() {

    private lateinit var mainNavController: NavController
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_container_main)
                as NavHostFragment
        mainNavController = navHostFragment.navController

        binding.bottomNav.setupWithNavController(mainNavController)
    }
}