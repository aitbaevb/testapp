package ab.innovo.testapp.common

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class BaseFragment(@LayoutRes contentLayoutId: Int = 0) :
    Fragment(contentLayoutId) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onInitUI(savedInstanceState == null)
        onObserveViewModel()
        onSetOnClickListeners()
    }

    protected open fun onObserveViewModel() {}

    protected open fun onInitUI(firstInit: Boolean) {}

    protected open fun onSetOnClickListeners() {}

}