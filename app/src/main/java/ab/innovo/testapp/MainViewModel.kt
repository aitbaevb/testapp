package ab.innovo.testapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel: ViewModel() {

    private val _percentage = MutableLiveData<Int>()
    val percentage: LiveData<Int> get() = _percentage

    fun setPercentage(percent: Int) {
        _percentage.value = percent
    }

}